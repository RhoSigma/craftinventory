importPath=string.gsub(getfenv(1)._.Name,"%.Main","").."."
resourcePath=string.gsub(importPath,"%.","/").."Resources/"

-- turbine client imports
import "Turbine";
import "Turbine.Gameplay";
import "Turbine.UI";
import "Turbine.UI.Lotro";
-- need localPlayer to test for session play
localPlayer=Turbine.Gameplay.LocalPlayer.GetInstance();
charName=localPlayer:GetName();
charVaultName=charName.." (Vault)";
-- mmediately test for session play BEFORE importing any other files so that we can safely bail without loading any data and without any handlers
if string.sub(charName,1,1)=="~" then
	-- do not load if the character is a session play character
	Turbine.Shell.WriteLine("Alt Inventory does not support session play characters.");
	return
end

-- get player inventory
backpack = Turbine.Gameplay.LocalPlayer:GetInstance():GetBackpack()

professionKeywords = {
	["Universal"] = {
		["hit"] = {
			"shard",
			{"crest", "of rohan"},
			{"symbol", "celebrimbor"},
		},
		["except"] = {},
	},
	["Cook"] = {
		["hit"] = {
			"cook",
			{"compendium", "craft"},
		},
		["except"] = {
			"scholar recipe",
			"food",
		},
	},
	["Farmer"] = {
		["hit"] = {
			"farmer",
			"farming",
			"crop",
			"harvest",
			"barley",
		},
		["except"] = {
			"scholar recipe",
			"map",
			"tools",
		},
	},
	["Forester"] = {
		["hit"] = {
			"forester",
			{"log", "wood"},
			{"wax", "wood"},
		},
		["except"] = {
			"scholar recipe",
			"chopping", -- for axes
		},
	},
	["Jeweller"] = {
		["hit"] = {
			"jeweller",
			"gem",
			"ingot",
			{"shaving", "ingot"},
			{"compendium", "craft"},
		},
		["except"] = {
			"scholar recipe",
			"tools",
		},
	},
	["Metalsmith"] = {
		["hit"] = {
			"metalsmith",
			"ingot",
			{"shaving", "ingot"},
			{"compendium", "craft"},
		},
		["except"] = {
			"scholar recipe",
			"gold",
			"silver",
			"platinum",
		},
	},
	["Prospector"] = {
		["hit"] = {
			"prospector",
			{"chunk", "ore"},
		},
		["except"] = {
			"brimstone",
			"scholar recipe",
			"axe",
			"tools",
			"compendium",
		},
	},
	["Scholar"] = {
		["hit"] = {
			"scholar",
			{"compendium", "craft"},
		},
		["except"] = {
			"crystal lens", -- for scholar's glass
			"tailor recipe", -- armour set is called scholar's armour
		},
	},
	["Tailor"] = {
		["hit"] = {
			"tailor",
			"leather shaving",
			{"leather", "arms and armour"},
			{"compendium", "craft"},
		},
		["except"] = {
			"scholar recipe",
			"branch",
			"tools",
		},
	},
	["Weaponsmith"] = {
		["hit"] = {
			"weaponsmith",
			"leather wrapping",
			"ingot",
			{"shaving", "ingot"},
			{"compendium", "craft"},
		},
		["except"] = {
			"scholar recipe",
			"gold",
			"silver",
			"platinum",
		},
	},
	["Woodworker"] = {
		["hit"] = {
			"woodworker",
			"woodworking",
			"resin",
			"leather wrapping",
			{"board", "weapons"},
			{"compendium", "craft"},
		},
		["except"] = {
			"scholar recipe",
			"branch",
			"tools", -- for woodworker tools
		},
	},
	["Exception"] = {
		"acceleration", -- no acceleration tomes
		"guide", -- no basic guides
	},
}

-- search for associated profession keywords in given item
GetProfessionByDescription = function(item, profession)
	if not item then return end
	local desc = string.lower(item:GetItemInfo():GetDescription() .. item:GetName())

	-- check for Universal exceptions
	local keywords = professionKeywords["Exception"]
	if not keywords then return end
	for k, keyword in pairs(keywords) do
		if string.find(desc, keyword) then return false end
	end

	local keywords = professionKeywords[profession]
	if not keywords then return end

	-- exceptions
	for k, keyword in pairs(keywords.except) do
		if string.find(desc, keyword) then return false end
	end

	-- hits
	for k, keyword in pairs(keywords.hit) do
		if type(keyword) == "string" then -- just a string comparison
			if string.find(desc, keyword) then return true end
		elseif type(keyword) == "table" then -- table comparsion, all must be found
			local hitAll = true
			for k2, keyword2 in pairs(keyword) do
				if not string.find(desc, keyword2) then hitAll = false end
			end
			if hitAll then
				return true
			end
		end
	end
	return false
end

-- scan inventory for items that belong to given profession
GetCraftItemsOfProfession = function(profession)
	local items = {}
	for i=1, backpack:GetSize() do
		local item = backpack:GetItem(i)
		if item then
			if GetProfessionByDescription(item, profession) then
				items[#items+1] = item
			end
		end
	end
	if #items < 1 then return nil end
	return items
end

-- for each profession, sort inventory and get item sub list
professionList = {
	[1] = "Universal",
	[2] = "Cook",
	[3] = "Farmer",
	[4] = "Forester",
	[5] = "Jeweller",
	[6] = "Metalsmith",
	[7] = "Prospector",
	[8] = "Scholar",
	[9] = "Tailor",
	[10] = "Weaponsmith",
	[11] = "Woodworker",
}

CreateItemList = function(name, colour, items)
	local ProfWindow=Turbine.UI.Control();
	ProfWindow:SetSize(400,40);
	ProfWindow:SetBackColor(colour);
	-- ProfWindow:SetBlendMode(Turbine.UI.BlendMode.Color);
	-- ProfWindow:SetBackground("RhoSigma/CraftHighlight/Resources/who_filteroptions_bg.tga");
	ProfWindow:SetMouseVisible(false);


	local ProfCaption=Turbine.UI.Label();
	ProfCaption:SetParent(ProfWindow);
	ProfCaption:SetSize(145,20);
	ProfCaption:SetPosition(0,0);
	ProfCaption:SetFont(Turbine.UI.Lotro.Font.Verdana18);
	ProfCaption:SetTextAlignment(Turbine.UI.ContentAlignment.MiddleLeft);
	ProfCaption:SetOutlineColor(Turbine.UI.Color(0,0,0));
	ProfCaption:SetFontStyle(Turbine.UI.FontStyle.Outline);
	ProfCaption:SetText(name);

	-- create item list
	local ItemList = Turbine.UI.ListBox()
	ItemList:SetMouseVisible(false);
	ItemList:SetParent(ProfWindow);
	ItemList:SetPosition(6,28);
	ItemList:SetSize(400,0);
	ItemList:SetMaxItemsPerLine(15);
	ItemList:SetOrientation(Turbine.UI.Orientation.Vertical);

	-- generate item layout and populate list
	local PopulateItemList = function(items)
		ItemList:ClearItems()
		CreateRowList = function()
			local rowList = Turbine.UI.ListBox()
			rowList:SetMouseVisible(false);
			ItemList:AddItem(rowList)
			rowList:SetMaxItemsPerLine(10);
			local listSize = ItemList:GetSize();
			ItemList:SetSize(ItemList:GetWidth(),ItemList:GetHeight()+38);
			rowList:SetSize(400,38);
			rowList:SetOrientation(Turbine.UI.Orientation.Horizontal);
			return rowList
		end
		local rowList = nil
		for k,listItem in ipairs(items) do
			if not rowList then rowList = CreateRowList() end
			if rowList:GetItemCount() > 10 then rowList = CreateRowList() end
			local Item = Turbine.UI.Lotro.ItemControl(listItem)
			rowList:AddItem(Item)
		end
	end
	PopulateItemList(items)
	ProfWindow:SetSize(400,ItemList:GetHeight()+38);
	return ProfWindow
end

colours = {
	[1] = Turbine.UI.Color(.04,.04,.04),
	[2] = Turbine.UI.Color(.09,.09,.09),
}

PopulateProfessionList = function()
	CraftInventoryWindow.ProfessionList:ClearItems()
	for index, profession in ipairs(professionList) do
		local items = GetCraftItemsOfProfession(profession)
		if items then
			if #items > 0 then
				local continue = true
				for k,listItem in ipairs(items) do
					if not listItem then
						continue = false
					end
				end
				if continue then
					local colour = nil
					if (CraftInventoryWindow.ProfessionList:GetItemCount() % 2 == 0) then
						colour = colours[1]
					else
						colour = colours[2]
					end
					local itemList = CreateItemList(profession, colour, items)
					if itemList then
						CraftInventoryWindow.ProfessionList:AddItem(itemList)
					end
				end
			end
		end
	end
end

-- handle object callbacks
function AddCallback(object, event, callback)
	if (object[event] == nil) then
		object[event] = callback;
	else
		if (type(object[event]) == "table") then
			local exists=false;
			local k,v;
			for k,v in ipairs(object[event]) do
				if v==callback then
					exists=true;
					break;
				end
			end
			if not exists then
				table.insert(object[event], callback);
			end
		else
			if object[event]~=callback then
				object[event] = {object[event], callback};
			end
		end
	end
	return callback;
end
-- safely remove a callback without clobbering any extras
function RemoveCallback(object, event, callback)
    if (object[event] == callback) then
        object[event] = nil;
    else
        if (type(object[event]) == "table") then
            local size = table.getn(object[event]);
            for i = 1, size do
                if (object[event][i] == callback) then
                    table.remove(object[event], i);
                    break;
                end
            end
        end
    end
end

ShouldUpdate = function()
	shouldUpdate = true
end

CreateWindow = function()
	-- create main window
	CraftInventoryWindow=Turbine.UI.Lotro.Window();
	-- CraftInventoryWindow:SetResizable(true);
	CraftInventoryWindow:SetSize(450,600);
	CraftInventoryWindow:SetPosition(Turbine.UI.Display:GetWidth()/2-100,Turbine.UI.Display:GetHeight()/2-100); -- centers the window in the display
	CraftInventoryWindow:SetText("Craft inventory"); -- assigns the title bar text
	-- CraftInventoryWindow:SetBlendMode(Turbine.UI.BlendMode.Overlay);
	-- CraftInventoryWindow:SetBackColor(Turbine.UI.Color(1,1,1))
	-- CraftInventoryWindow:SetBackground("RhoSigma/CraftHighlight/Resources/CTL_BG.jpg");

	-- background
	CraftInventoryWindow.BackgroundImg=Turbine.UI.Control();
	CraftInventoryWindow.BackgroundImg:SetParent(CraftInventoryWindow);
	CraftInventoryWindow.BackgroundImg:SetZOrder(-1);
	CraftInventoryWindow.BackgroundImg:SetSize(478,600);
	CraftInventoryWindow.BackgroundImg:SetPosition(0,0);
	CraftInventoryWindow.BackgroundImg:SetBlendMode(Turbine.UI.BlendMode.Screen);
	CraftInventoryWindow.BackgroundImg:SetBackground("RhoSigma/CraftHighlight/Resources/CTL_BG.jpg");
	CraftInventoryWindow.BackgroundImg:SetMouseVisible(false);

	-- main profession panel
	CraftInventoryWindow.ProfessionList=Turbine.UI.ListBox()
	CraftInventoryWindow.ProfessionList:SetMouseVisible(true);
	CraftInventoryWindow.ProfessionList:SetParent(CraftInventoryWindow)
	CraftInventoryWindow.ProfessionList:SetPosition(25,50);
	CraftInventoryWindow.ProfessionList:SetSize(400,500);
	CraftInventoryWindow.ProfessionList:SetMaxItemsPerLine(15);
	CraftInventoryWindow.ProfessionList:SetOrientation(Turbine.UI.Orientation.Vertical);

	-- main profession panel scrollbar
	CraftInventoryWindow.ProfessionListScrollBar = Turbine.UI.Lotro.ScrollBar();
	CraftInventoryWindow.ProfessionListScrollBar:SetParent(CraftInventoryWindow);
	CraftInventoryWindow.ProfessionListScrollBar:SetOrientation( Turbine.UI.Orientation.Vertical );
	CraftInventoryWindow.ProfessionList:SetVerticalScrollBar(CraftInventoryWindow.ProfessionListScrollBar);
	CraftInventoryWindow.ProfessionListScrollBar:SetPosition(425, 50);
	CraftInventoryWindow.ProfessionListScrollBar:SetSize(10, 500);

	CraftInventoryWindow:SetWantsKeyEvents(true);
	CraftInventoryWindow.KeyDown = function(sender, args)
		if (args.Action == Turbine.UI.Lotro.Action.Escape) then
			CraftInventoryWindow:SetVisible(false)
			CraftInventoryWindow:SetWantsUpdates(false)
			RemoveCallback(backpack,"ItemRemoved",ShouldUpdate)
			RemoveCallback(backpack,"ItemAdded",ShouldUpdate)
		end
		if Turbine.UI.Control.IsAltKeyDown() then
			if (args.Action == Turbine.UI.Lotro.Action.ToggleBags) then -- Hide if ESC key is press
				CraftInventoryWindow:SetVisible(not CraftInventoryWindow:IsVisible());
				if CraftInventoryWindow:IsVisible() then
					CraftInventoryWindow:SetWantsUpdates(true);
					PopulateProfessionList()
					AddCallback(backpack,"ItemRemoved",ShouldUpdate)
					AddCallback(backpack,"ItemAdded",ShouldUpdate)
				else
					CraftInventoryWindow:SetWantsUpdates(false);
					RemoveCallback(backpack,"ItemRemoved",ShouldUpdate)
					RemoveCallback(backpack,"ItemAdded",ShouldUpdate)
				end
			end
		end
	end

	CraftInventoryWindow.Update = function(sender, args)
		if shouldUpdate then
			if not frameNumber then frameNumber = 1 end
			frameNumber = frameNumber + 1
			if frameNumber > 15 then
				frameNumber = 0
				shouldUpdate = false
				PopulateProfessionList()
			end
		end
	end
end

CreateWindow()
--
-- CraftInventoryWindow:SetVisible(true)
-- PopulateProfessionList()